# ControlTotal ejemplo de integración API-js

A continuación encontrarás un ejemplo de integración básico para php


Asegúrate de reemplazar los valores de usuario y contraseña en getToken.php

Recuerda también que esto es solo un ejemplo debes personalizarlo y adaptarlo según las características específicas de tu proyecto.

####IMPORTANTE: Este ejemplo no maneja validaciones, ni controla vigencia del token.


El objetivo de este ejemplo es mostrar como se deben crear los endpoints (proxy) que redirigirán internamente las peticiones realizadas a tu aplicacion, hacia los endpoints de ControlTotal, garantizando que tu token de acceso, siempre esté seguro y protegido ya que solo se tendrá acceso a él desde el back de tu plataforma.

## Instalación

Estas son las instrucciones para instalar y configurar el ejemplo en una máquina local.

1. Clona este repositorio en la carpeta de tu servidor:
git clone https://gitlab.com/gurusistemas/controltotal-api-js-php-sample

2. Ingresa al directorio del proyecto:
cd controltotal-api-js-php-sample

3. Actualiza los scripts con el endpoint correcto, y usuario y contraseña

Este proyecto incluye endpoints que actúan como intermediarios entre tu aplicación web y la API de ControlTotal.

La interfaz web se encuentra en el archivo html/index.html.
