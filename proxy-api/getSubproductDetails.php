<?php
header("access-control-allow-origin: *");
header("access-control-allow-methods: GET,HEAD,OPTIONS,POST,PUT,DELETE");
header("access-control-allow-headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,authorization,content-type");

$apiUrl = 'https://api-sandbox.controltotal.vip/products/getSubproductDetails';

// Usar token de acceso almacenado
$accessToken = "...";

// Obtener todos los parámetros de la URL
$userParams = $_GET;

// Configurar la solicitud con cURL
$ch = curl_init($apiUrl . '?' . http_build_query($userParams)); // Añadir todos los parámetros a la URL
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Authorization: Bearer ' . $accessToken,
]);

// Realizar la solicitud GET
$response = curl_exec($ch);

// Verificar si la solicitud fue exitosa
if (curl_errno($ch)) {
    echo 'Error en la solicitud: ' . curl_error($ch);
} else {
    // Manejar la respuesta exitosa
    echo $response;
}

// Cerrar la sesión cURL
curl_close($ch);
