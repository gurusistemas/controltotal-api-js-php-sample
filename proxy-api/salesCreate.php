<?php
header("access-control-allow-origin: *");
header("access-control-allow-methods: GET,HEAD,OPTIONS,POST,PUT,DELETE");
header("access-control-allow-headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,authorization,content-type");

$apiUrl = 'https://api-sandbox.controltotal.vip/sales/create';

// Usar token de acceso almacenado
$accessToken = "...";

// Obtener los parámetros del cuerpo de la solicitud del usuario
$userParams = $_POST;

// Si no hay datos en $_POST, intentar obtenerlos del cuerpo del mensaje
if (empty($userParams)) {
  $rawPostData = file_get_contents("php://input");
  $userParams = json_decode($rawPostData, true);
}
// Codificar los parámetros en formato JSON (ajusta esto según la estructura de tus datos)
$jsonParams = json_encode($userParams);

// Configurar la solicitud con cURL
$ch = curl_init($apiUrl);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonParams);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
  'Authorization: Bearer ' . $accessToken,
  'Content-Type: application/json',
]);

// Realizar la solicitud GET
$response = curl_exec($ch);

// Verificar si la solicitud fue exitosa
if (curl_errno($ch)) {
    echo 'Error en la solicitud: ' . curl_error($ch);
} else {
    // Manejar la respuesta exitosa
    echo $response;
}

// Cerrar la sesión cURL
curl_close($ch);
