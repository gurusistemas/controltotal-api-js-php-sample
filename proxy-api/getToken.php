<?php
header("access-control-allow-origin: *");
header("access-control-allow-methods: GET,HEAD,OPTIONS,POST,PUT,DELETE");
header("access-control-allow-headers: access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,authorization,content-type");

// Nombre de usuario y contraseña del usuario para API
$username = "tu_id_de_usuario";
$password = "tu_contraseña";

// Definir la URL de destino
$apiUrl = 'https://api-sandbox.controltotal.vip';

// Codificar las credenciales en Base64
$credentials = base64_encode("$username:$password");

// Configurar la solicitud con cURL
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "$apiUrl/auth/sign-in");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: text/plain',
    'Authorization: Basic ' . $credentials,
));
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['token_lifetime' => 84600]));

// Realizar la solicitud de autenticación
$response = curl_exec($ch);

// Verificar si la solicitud fue exitosa
if (curl_errno($ch)) {
    echo 'Error en la autenticación: ' . curl_error($ch);
    curl_close($ch);
    exit;
}

// Decodificar la respuesta JSON
$data = json_decode($response, true);

// Recibir el token de acceso desde la respuesta
$accessToken = $data['access_token'];

// ALMACENAR EL TOKEN de tal manera que puedas usarlo en las peticiones

// Cerrar la sesión cURL
curl_close($ch);